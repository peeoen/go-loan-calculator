package ballooneffectiverate

import (
	"go-loan-calculator/model"
	"testing"
)

// Calculation Sheet Include Tax: True
// Calculation Sheet Include Additional Cost: true
// FV Formular: Balloon
// PV Formular: Cost
// Calculation Sheet Interest Rate Type : Effective Rate
// Rounding Method: Exclude Tax
// Rounding Type: Round up
// Round Payment Amount: 10

// ----------------------------------------------------------

// Down: 20%
// Deposit: 0%
// R.V.: 0%
// Balloon Payment: 25%
// Repurchase Amount: 0
// Advance Payment: 0

func Example1() {
	options := getPaymentScheduleOptions()
	total_cost := Total(options.AdditionalsCost)
	options.PaymentOption.CalPayment(total_cost)

	fv, err := options.GetFV()

	_ = fv
	_ = err
	_ = options
}

func getPaymentScheduleOptions() model.PaymentSchuduleOption {

	rate := model.Rate{
		FlatRate: 7,
	}
	leaseTypeSetup := model.LeaseTypeSetup{
		CalSheetTax:     true,
		CalSheetAddCost: true,
		FvFormular:      model.FvFormular.Balloon,
		PvFormular:      model.PvFormular.Cost,
		CalSheetRate:    model.CalSheetRate.EffectiveRate,
		RoundMethod:     model.Round.RoundUp,
		RoundPayment:    10,
	}

	paymentOption := model.PaymentOption{
		DownPercent:       20,
		DepositPercent:    0,
		RVPercent:         0,
		BalloonPercent:    25,
		RepurchasePercent: 0,
		AdvancePayment:    0,
	}

	vehicleTax := 350.0
	propertyCost := 1070000.0

	costs := model.AdditionalsCost{
		VehicleTaxFee: vehicleTax,
		PropertyCost:  propertyCost,
		Costs: []model.Cost{
			{
				CostType:   "Property Cost",
				CostAmount: propertyCost,
			},
			{
				CostType:   "Insurance",
				CostAmount: 10807,
			},
			{
				CostType:   "Compulsory",
				CostAmount: 695.5,
			},
			{
				CostType:   "Vehicle Tax Fee",
				CostAmount: vehicleTax,
			},
			{
				CostType:   "Vehicle Tax Service Fee",
				CostAmount: 53.5,
			},
			{
				CostType:   "Maintenance",
				CostAmount: 1070,
			},
			{
				CostType:   "Other cost ",
				CostAmount: 107,
			},
		},
	}

	taxOption := model.TaxOption{
		CostTax:        7,
		InstallmentTax: 7,
		WHT:            5,
	}

	termOption := model.TermOption{
		Term:        36,
		PaymentType: model.PaymentType.Ending,
		PeriodGap:   model.PeriodGap.Quarterly,
	}

	paymentOptions := model.PaymentSchuduleOption{
		LeaseTypeSetup:  leaseTypeSetup,
		PaymentOption:   paymentOption,
		AdditionalsCost: costs,
		TaxOption:       taxOption,
		TermOption:      termOption,
		Rate:            rate,
	}

	return paymentOptions
}

func getPaymentScheduleQuarterlyOptions() model.PaymentSchuduleOption {

	rate := model.Rate{
		// FlatRate: 7,
		EffectiveRate: 6.5,
	}
	leaseTypeSetup := model.LeaseTypeSetup{
		CalSheetTax:     false,
		CalSheetAddCost: false,
		FvFormular:      model.FvFormular.Balloon,
		PvFormular:      model.PvFormular.Cost,
		CalSheetRate:    model.CalSheetRate.EffectiveRate,
		RoundMethod:     model.Round.RoundUp,
		RoundPayment:    1,
	}

	paymentOption := model.PaymentOption{
		DownPercent:       20,
		DepositPercent:    0,
		RVPercent:         0,
		BalloonPercent:    0,
		RepurchasePercent: 0,
		AdvancePayment:    0,
	}

	vehicleTax := 0.0
	propertyCost := 31693457.90

	costs := model.AdditionalsCost{
		VehicleTaxFee: vehicleTax,
		PropertyCost:  propertyCost,
		Costs: []model.Cost{
			{
				CostType:   "Property Cost",
				CostAmount: propertyCost,
			},
		},
	}

	taxOption := model.TaxOption{
		CostTax:        7,
		InstallmentTax: 7,
		WHT:            0,
	}

	termOption := model.TermOption{
		Term:        36,
		PaymentType: model.PaymentType.Ending,
		PeriodGap:   model.PeriodGap.Quarterly,
	}

	paymentOptions := model.PaymentSchuduleOption{
		LeaseTypeSetup:  leaseTypeSetup,
		PaymentOption:   paymentOption,
		AdditionalsCost: costs,
		TaxOption:       taxOption,
		TermOption:      termOption,
		Rate:            rate,
	}

	return paymentOptions
}

func getPaymentScheduleBeginningMonthlyOptions() model.PaymentSchuduleOption {

	rate := model.Rate{
		FlatRate: 5,
		// EffectiveRate: 6.5,
	}
	leaseTypeSetup := model.LeaseTypeSetup{
		CalSheetTax:     false,
		CalSheetAddCost: false,
		FvFormular:      model.FvFormular.Balloon,
		PvFormular:      model.PvFormular.Cost,
		CalSheetRate:    model.CalSheetRate.FlatRate,
		RoundMethod:     model.Round.RoundUp,
		RoundPayment:    1,
	}

	paymentOption := model.PaymentOption{
		DownPercent:       0,
		DepositPercent:    0,
		RVPercent:         0,
		BalloonPercent:    0,
		RepurchasePercent: 0,
		AdvancePayment:    0,
	}

	vehicleTax := 0.0
	propertyCost := 1000000.0

	costs := model.AdditionalsCost{
		VehicleTaxFee: vehicleTax,
		PropertyCost:  propertyCost,
		Costs: []model.Cost{
			{
				CostType:   "Property Cost",
				CostAmount: propertyCost,
			},
			{
				CostType:   "Insurance",
				CostAmount: 10000,
			},
		},
	}

	taxOption := model.TaxOption{
		CostTax:        7,
		InstallmentTax: 7,
		WHT:            0,
	}

	termOption := model.TermOption{
		Term:        12,
		PaymentType: model.PaymentType.Beginning,
		PeriodGap:   model.PeriodGap.Monthly,
	}

	paymentOptions := model.PaymentSchuduleOption{
		LeaseTypeSetup:  leaseTypeSetup,
		PaymentOption:   paymentOption,
		AdditionalsCost: costs,
		TaxOption:       taxOption,
		TermOption:      termOption,
		Rate:            rate,
	}

	return paymentOptions
}

func getPaymentScheduleBeginningQuarterlyOptions() model.PaymentSchuduleOption {

	rate := model.Rate{
		FlatRate: 5,
		// EffectiveRate: 6.5,
	}
	leaseTypeSetup := model.LeaseTypeSetup{
		CalSheetTax:     false,
		CalSheetAddCost: false,
		FvFormular:      model.FvFormular.Balloon,
		PvFormular:      model.PvFormular.Cost,
		CalSheetRate:    model.CalSheetRate.FlatRate,
		RoundMethod:     model.Round.RoundUp,
		RoundPayment:    1,
	}

	paymentOption := model.PaymentOption{
		DownPercent:       30,
		DepositPercent:    0,
		RVPercent:         0,
		BalloonPercent:    0,
		RepurchasePercent: 0,
		AdvancePayment:    0,
	}

	vehicleTax := 0.0
	propertyCost := 500000.0

	costs := model.AdditionalsCost{
		VehicleTaxFee: vehicleTax,
		PropertyCost:  propertyCost,
		Costs: []model.Cost{
			{
				CostType:   "Property Cost",
				CostAmount: propertyCost,
			},
		},
	}

	taxOption := model.TaxOption{
		CostTax:        7,
		InstallmentTax: 7,
		WHT:            0,
	}

	termOption := model.TermOption{
		Term:        48,
		PaymentType: model.PaymentType.Beginning,
		PeriodGap:   model.PeriodGap.Quarterly,
	}

	paymentOptions := model.PaymentSchuduleOption{
		LeaseTypeSetup:  leaseTypeSetup,
		PaymentOption:   paymentOption,
		AdditionalsCost: costs,
		TaxOption:       taxOption,
		TermOption:      termOption,
		Rate:            rate,
	}

	return paymentOptions
}

func TestExampleFV(t *testing.T) {
	options := getPaymentScheduleOptions()
	total_cost := Total(options.AdditionalsCost)
	options.PaymentOption.CalPayment(total_cost)

	fv, err := options.GetFV()

	_ = fv
	_ = err
	_ = options
}
