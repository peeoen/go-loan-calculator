package ballooneffectiverate

import (
	"fmt"
	"math"
	"testing"

	"github.com/fatih/color"
	"github.com/rodaine/table"
)

// Calculation Sheet Include Tax: True
// Calculation Sheet Include Additional Cost: true
// FV Formular: Balloon
// PV Formular: Cost
// Calculation Sheet Interest Rate Type : Effective Rate
// Rounding Method: Exclude Tax
// Rounding Type: Round up
// Round Payment Amount: 10

// ----------------------------------------------------------

// Down: 20%
// Deposit: 0%
// R.V.: 0%
// Balloon Payment: 25%
// Repurchase Amount: 0
// Advance Payment: 0
func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}

func TestExample11(t *testing.T) {
	// printer := tableprinter.New(os.Stdout)

	options := getPaymentScheduleBeginningQuarterlyOptions()
	total_cost := options.TotalCost()
	options.PaymentOption.CalPayment(options.AdditionalsCost.PropertyCost)

	fv, err := options.GetFV()
	pv, err := options.GetPV()
	instllment, err := options.GetInstallment()
	irr_rate, err := options.GetIrrRate()

	data, err := options.GetPaymentSchedule()

	_ = data
	_ = options
	_ = err
	_ = total_cost
	_ = fv
	_ = pv
	_ = irr_rate
	_ = instllment
	// fmt.Printf("data")
	// go_tableprinter.SetSortedHeaders(false)
	// go_tableprinter.Print(data)
	// printer.CenterSeparator = "│"
	// printer.ColumnSeparator = "│"
	// printer.RowSeparator = "─"
	// printer.HeaderBgColor = tablewriter.BgBlackColor
	// printer.HeaderFgColor = tablewriter.FgGreenColor
	// printer.Print(data)

	headerFmt := color.New(color.FgGreen, color.Underline).SprintfFunc()
	columnFmt := color.New(color.FgYellow).SprintfFunc()

	tbl := table.New("Period", "LineType", "Payment", "Interest", "Principal", "OutstandingPrincipal")
	tbl.WithHeaderFormatter(headerFmt).WithFirstColumnFormatter(columnFmt)

	for _, d := range data {
		payment := fmt.Sprintf("%.2f", d.Payment)
		interest := fmt.Sprintf("%.2f", d.Interest)
		principal := fmt.Sprintf("%.2f", d.Principal)
		outstandingPrincipal := fmt.Sprintf("%.2f", d.OutstandingPrincipal)

		tbl.AddRow(d.Period, d.LineType, payment, interest, principal, outstandingPrincipal)
	}

	tbl.Print()

	fmt.Printf("data")
	// paymentSchedules := make([]model.PaymentSchedule, 0)
	// lineType := "Installment"
	// outstanding := pv

	// for i := 0; i <= options.TermOption.Term; i++ {
	// 	if i == 0 {
	// 		ar_amount := adjusted_net_payment_amount*(float64(options.TermOption.Term)/float64(options.TermOption.PeriodGap)-0) + fv*(1+options.TaxOption.InstallmentTax/100)
	// 		paymentSchedules = append(paymentSchedules, model.PaymentSchedule{
	// 			Period:               i,
	// 			LineType:             lineType,
	// 			OutstandingPrincipal: outstanding,
	// 			ARAmount:             finance.Round(ar_amount, 2),
	// 		})
	// 	} else if i == options.TermOption.Term { // last row

	// 	} else { // not 1 and last

	// 		interest := outstanding * irr_value / (12.0 / float64(options.TermOption.PeriodGap))
	// 		pricipal := adjusted_payment_amount_rounded - interest
	// 		outstanding = outstanding - pricipal
	// 		paymentSchedules = append(paymentSchedules, model.PaymentSchedule{
	// 			Period:                 i,
	// 			LineType:               lineType,
	// 			Payment:                adjusted_payment_amount_rounded,
	// 			Interest:               interest,
	// 			Principal:              pricipal,
	// 			OutstandingPrincipal:   outstanding,
	// 			ARAmount:               0,
	// 			TaxAmount:              0,
	// 			PaymentIncludeSalesTax: 0,
	// 			WHTAmount:              0,
	// 		})
	// 	}
	// }

}
