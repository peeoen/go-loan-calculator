package ballooneffectiverate

import (
	"fmt"
	"go-loan-calculator/model"
	"math"
	"testing"

	finance "go-loan-calculator/alpeb-go-finance"

	"github.com/chrusty/go-tableprinter"
)

// Calculation Sheet Include Tax: True
// Calculation Sheet Include Additional Cost: true
// FV Formular: Balloon
// PV Formular: Cost
// Calculation Sheet Interest Rate Type : Effective Rate
// Rounding Method: Exclude Tax
// Rounding Type: Round up
// Round Payment Amount: 10

// ----------------------------------------------------------

// Down: 20%
// Deposit: 0%
// R.V.: 0%
// Balloon Payment: 25%
// Repurchase Amount: 0
// Advance Payment: 0

func TestExample1(t *testing.T) {

	leaseTypeSetup := model.LeaseTypeSetup{
		CalSheetTax:     true,
		CalSheetAddCost: true,
		FvFormular:      model.FvFormular.Balloon,
		PvFormular:      model.PvFormular.Cost,
		CalSheetRate:    model.CalSheetRate.EffectiveRate,
		RoundMethod:     model.Round.RoundUp,
		RoundPayment:    10,
	}

	paymentOptions := model.PaymentOption{
		DownPercent:       20,
		DepositPercent:    0,
		RVPercent:         0,
		BalloonPercent:    25,
		RepurchasePercent: 0,
		AdvancePayment:    0,
	}

	propertyCost := float64(1070000)

	vehicleTax := 350.0
	costs := model.AdditionalsCost{
		VehicleTaxFee: vehicleTax,
		Costs: []model.Cost{
			{
				CostType:   "Property Cost",
				CostAmount: propertyCost,
			},
			{
				CostType:   "Insurance",
				CostAmount: 10807,
			},
			{
				CostType:   "Compulsory",
				CostAmount: 695.5,
			},
			{
				CostType:   "Vehicle Tax Fee",
				CostAmount: vehicleTax,
			},
			{
				CostType:   "Vehicle Tax Service Fee",
				CostAmount: 53.5,
			},
			{
				CostType:   "Maintenance",
				CostAmount: 1070,
			},
			{
				CostType:   "Other cost ",
				CostAmount: 107,
			},
		},
	}

	taxOption := model.TaxOption{
		CostTax:        7,
		InstallmentTax: 7,
		WHT:            5,
	}

	termOption := model.TermOption{
		Term:        36,
		PaymentType: model.PaymentType.Ending,
		PeriodGap:   model.PeriodGap.Monthly,
	}

	total_cost := Total(costs)

	paymentOptions.CalPayment(propertyCost)

	fv := paymentOptions.BalloonAmount / (1 + (float64(taxOption.InstallmentTax) / 100))

	costTax := 1 + float64(taxOption.CostTax)/100

	// p1 := (total_cost-costs.VehicleTaxFee)/costTax + costs.VehicleTaxFee
	// _ = p1

	pv := (total_cost-costs.VehicleTaxFee)/costTax + costs.VehicleTaxFee - (paymentOptions.DownAmount+paymentOptions.AdvancePayment)/costTax
	pv = math.Round(pv)

	effective_rate := 13.0
	effective_rate_cal := (effective_rate / 100) / (12.0 / float64(termOption.PeriodGap))

	adjusted_payment_amount, err := finance.Payment(effective_rate_cal,
		termOption.Term, -pv, fv, 0)

	adjusted_payment_amount_rounded := finance.Ceiling(adjusted_payment_amount, float64(leaseTypeSetup.RoundPayment))

	period := (termOption.Term / termOption.PeriodGap)

	irr, err := finance.Rate(period, adjusted_payment_amount_rounded, -pv, fv, termOption.PaymentType, 1)

	irr_value := irr * (12.0 / float64(termOption.PeriodGap))
	tax_amount := adjusted_payment_amount_rounded * taxOption.InstallmentTax / 100
	adjusted_net_payment_amount := adjusted_payment_amount_rounded + tax_amount
	wht_amount := adjusted_payment_amount_rounded * taxOption.WHT / 100
	payment_after_wht := adjusted_net_payment_amount - wht_amount

	paymentSchedules := make([]model.PaymentSchedule, 0)
	lineType := "Installment"
	outstanding := pv

	for i := 0; i <= termOption.Term; i++ {
		if i == 0 {
			ar_amount := adjusted_net_payment_amount*(float64(termOption.Term)/float64(termOption.PeriodGap)-0) + fv*(1+taxOption.InstallmentTax/100)
			paymentSchedules = append(paymentSchedules, model.PaymentSchedule{
				Period:               i,
				LineType:             lineType,
				OutstandingPrincipal: outstanding,
				ARAmount:             finance.Round(ar_amount, 2),
			})
		} else if i == termOption.Term { // last row

		} else { // not 1 and last

			interest := outstanding * irr_value / (12.0 / float64(termOption.PeriodGap))
			pricipal := adjusted_payment_amount_rounded - interest
			outstanding = outstanding - pricipal
			paymentSchedules = append(paymentSchedules, model.PaymentSchedule{
				Period:                 i,
				LineType:               lineType,
				Payment:                adjusted_payment_amount_rounded,
				Interest:               interest,
				Principal:              pricipal,
				OutstandingPrincipal:   outstanding,
				ARAmount:               0,
				TaxAmount:              0,
				PaymentIncludeSalesTax: 0,
				WHTAmount:              0,
			})
		}
	}
	_ = fv
	_ = pv
	_ = costTax
	_ = total_cost
	_ = adjusted_payment_amount
	_ = err
	_ = adjusted_payment_amount_rounded
	_ = irr
	_ = irr_value
	_ = paymentSchedules
	_ = tax_amount
	_ = adjusted_net_payment_amount
	_ = wht_amount
	_ = payment_after_wht

	fmt.Printf("%+v\n", leaseTypeSetup)
	fmt.Printf("%+v\n", paymentOptions)
	fmt.Printf("%+v\n", costs)
	fmt.Printf("%+v\n", taxOption)
	fmt.Printf("%+v\n", termOption)
	fmt.Printf("%+v\n", irr_value)
	tableprinter.SetSortedHeaders(false)
	tableprinter.Print(paymentSchedules)
	fmt.Printf("\n\n")
}

func GeneratePaymentSchedule() {

}

// func Total(add_cost model.AdditionalsCost) (result float64) {
// 	result = 0
// 	for _, cost := range add_cost.Costs {
// 		result += float64(cost.CostAmount)
// 	}
// 	return result
// }

func TestPMT(t *testing.T) {

	// test := (13.0 / 100.0) / (12.0 / 1.0)
	effective_rate := (13.0 / 100.0) / (12.0 / 1.0)
	periods := 36
	fv := float64(250000)
	pv := float64(812250)

	adjusted_payment_amount, err := finance.Payment(effective_rate, periods, -pv, fv, 0)

	_ = adjusted_payment_amount
	_ = err

	fmt.Printf("End Test")
}

func TestPrintSlice(t *testing.T) {
	data := []model.PaymentSchedule{
		{
			LineType: "test1",
		},
		{
			LineType: "test2",
		},
	}
	fmt.Printf("%+v\n", data)
}
