package ballooneffectiverate

import (
	"go-loan-calculator/model"
)

func Total(add_cost model.AdditionalsCost) (result float64) {
	result = 0
	for _, cost := range add_cost.Costs {
		result += float64(cost.CostAmount)
	}
	return result
}
