module go-loan-calculator

go 1.19

require github.com/chrusty/go-tableprinter v0.0.0-20190528113659-0de6c8f09400

require (
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/rodaine/table v1.0.1 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/fatih/color v1.13.0
	github.com/kataras/tablewriter v0.0.0-20180708051242-e063d29b7c23
	github.com/lensesio/tableprinter v0.0.0-20201125135848-89e81fc956e7
	github.com/lunux2008/xulu v0.0.0-20160308154621-fff51ca7218e
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/olekukonko/tablewriter v0.0.1 // indirect
)
