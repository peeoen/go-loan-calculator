package model

import (
	"errors"
	finance "go-loan-calculator/alpeb-go-finance"
	"math"
)

func (opt *PaymentSchuduleOption) TotalCost() float64 {
	result := 0.0
	for _, cost := range opt.AdditionalsCost.Costs {
		result += float64(cost.CostAmount)
	}
	return result
}

func (opt *PaymentSchuduleOption) GetFV() (float64, error) {

	if opt.LeaseTypeSetup.FvFormular == FvFormular.RV &&
		opt.LeaseTypeSetup.CalSheetTax == false {
		return opt.PaymentOption.RVAmount, nil
	} else if opt.LeaseTypeSetup.FvFormular == FvFormular.RVDeposite &&
		opt.LeaseTypeSetup.CalSheetTax == false {
		return opt.PaymentOption.RVAmount, nil
	} else if opt.LeaseTypeSetup.FvFormular == FvFormular.Repurchase &&
		opt.LeaseTypeSetup.CalSheetTax == false {
		return opt.PaymentOption.RepurchaseAmount, nil
	} else if opt.LeaseTypeSetup.FvFormular == FvFormular.Balloon &&
		opt.LeaseTypeSetup.CalSheetTax == false {
		return opt.PaymentOption.BalloonAmount, nil
	} else if opt.LeaseTypeSetup.FvFormular == FvFormular.RV &&
		opt.LeaseTypeSetup.CalSheetTax == true {
		return opt.PaymentOption.RVAmount / (1 + opt.TaxOption.InstallmentTax/100), nil
	} else if opt.LeaseTypeSetup.FvFormular == FvFormular.RVDeposite &&
		opt.LeaseTypeSetup.CalSheetTax == true {
		return opt.PaymentOption.RVAmount/(1+opt.TaxOption.InstallmentTax/100) - opt.PaymentOption.DepositAmount/(1+opt.TaxOption.CostTax/100), nil
	} else if opt.LeaseTypeSetup.FvFormular == FvFormular.Repurchase &&
		opt.LeaseTypeSetup.CalSheetTax == true {
		return opt.PaymentOption.RepurchaseAmount / (1 + opt.TaxOption.InstallmentTax/100), nil
	} else if opt.LeaseTypeSetup.FvFormular == FvFormular.Balloon &&
		opt.LeaseTypeSetup.CalSheetTax == true {
		return opt.PaymentOption.BalloonAmount / (1 + opt.TaxOption.InstallmentTax/100), nil
	} else {
		return 0, errors.New("Lease type invalid fields.")
	}
}

func (opt *PaymentSchuduleOption) GetPV() (float64, error) {
	totalCost := opt.TotalCost()
	pv := 0.0
	if opt.LeaseTypeSetup.CalSheetTax == false &&
		opt.LeaseTypeSetup.PvFormular == PvFormular.Cost &&
		opt.LeaseTypeSetup.CalSheetAddCost == true {
		pv = totalCost - opt.PaymentOption.DownAmount - opt.PaymentOption.AdvancePayment
	} else if opt.LeaseTypeSetup.CalSheetTax == false &&
		opt.LeaseTypeSetup.PvFormular == PvFormular.CostDeposite &&
		opt.LeaseTypeSetup.CalSheetAddCost == true {
		pv = totalCost - opt.PaymentOption.DownAmount - opt.PaymentOption.AdvancePayment
	} else if opt.LeaseTypeSetup.CalSheetTax == true &&
		opt.LeaseTypeSetup.PvFormular == PvFormular.Cost &&
		opt.LeaseTypeSetup.CalSheetAddCost == true {
		pv = (totalCost-opt.AdditionalsCost.VehicleTaxFee)/(1+opt.TaxOption.CostTax/100) + opt.AdditionalsCost.VehicleTaxFee - (opt.PaymentOption.DownAmount+opt.PaymentOption.AdvancePayment)/(1+opt.TaxOption.CostTax/100)
	} else if opt.LeaseTypeSetup.CalSheetTax == true &&
		opt.LeaseTypeSetup.PvFormular == PvFormular.CostDeposite &&
		opt.LeaseTypeSetup.CalSheetAddCost == true {
		pv = (totalCost-opt.AdditionalsCost.VehicleTaxFee)/(1+opt.TaxOption.CostTax/100) + opt.AdditionalsCost.VehicleTaxFee - (opt.PaymentOption.DownPercent+opt.PaymentOption.AdvancePayment+opt.PaymentOption.DepositAmount)/(1+opt.TaxOption.CostTax/100)
	} else if opt.LeaseTypeSetup.CalSheetTax == false &&
		opt.LeaseTypeSetup.PvFormular == PvFormular.Cost &&
		opt.LeaseTypeSetup.CalSheetAddCost == false {
		pv = opt.AdditionalsCost.PropertyCost - opt.PaymentOption.DownAmount - opt.PaymentOption.AdvancePayment
	} else if opt.LeaseTypeSetup.CalSheetTax == false &&
		opt.LeaseTypeSetup.PvFormular == PvFormular.CostDeposite &&
		opt.LeaseTypeSetup.CalSheetAddCost == false {
		pv = opt.AdditionalsCost.PropertyCost - opt.PaymentOption.DownAmount - opt.PaymentOption.AdvancePayment - opt.PaymentOption.DepositAmount
	} else if opt.LeaseTypeSetup.CalSheetTax == true &&
		opt.LeaseTypeSetup.PvFormular == PvFormular.Cost &&
		opt.LeaseTypeSetup.CalSheetAddCost == false {
		pv = opt.AdditionalsCost.PropertyCost/(1+opt.TaxOption.CostTax/100) - (opt.PaymentOption.DownAmount+opt.PaymentOption.AdvancePayment)/(1+opt.TaxOption.CostTax/100)
	} else if opt.LeaseTypeSetup.CalSheetTax == true &&
		opt.LeaseTypeSetup.PvFormular == PvFormular.CostDeposite &&
		opt.LeaseTypeSetup.CalSheetAddCost == false {
		pv = opt.AdditionalsCost.PropertyCost/(1+opt.TaxOption.CostTax/100) - (opt.PaymentOption.DownAmount+opt.PaymentOption.AdvancePayment+opt.PaymentOption.DepositAmount)/(1+opt.TaxOption.CostTax/100)
	} else {
		return pv, errors.New("Lease type invalid fields.")
	}
	return ToFixed(pv, 2), nil
}

func (cal *PaymentOption) CalPayment(total_cost float64) {
	if cal.DownPercent != 0 {
		cal.DownAmount = total_cost * float64(cal.DownPercent) / 100
	}
	if cal.DepositPercent != 0 {
		cal.DepositAmount = total_cost * float64(cal.DepositPercent) / 100
	}
	if cal.RVPercent != 0 {
		cal.RVAmount = total_cost * float64(cal.RVPercent) / 100
	}
	if cal.BalloonPercent != 0 {
		cal.BalloonAmount = total_cost * float64(cal.BalloonPercent) / 100
	}
	if cal.RepurchasePercent != 0 {
		cal.RepurchaseAmount = total_cost * float64(cal.RepurchasePercent) / 100
	}
}

func (opt *PaymentSchuduleOption) GetIrrRate() (float64, error) {
	pv, _ := opt.GetPV()
	fv, _ := opt.GetFV()

	if opt.Rate.EffectiveRate > 0 { // effective rate
		// return opt.Rate.EffectiveRate / 100, nil
		numPeriods := opt.TermOption.Term / opt.TermOption.PeriodGap
		installment_rounded, _ := opt.GetInstallment()
		rate, _ := finance.Rate(numPeriods, installment_rounded, -pv, fv, opt.TermOption.PaymentType, 1)
		rate = rate * 12 / float64(opt.TermOption.PeriodGap)
		rate = ToFixed(rate, 12)
		return rate, nil
	} else if opt.Rate.FlatRate > 0 { // flat rate
		numPeriods := opt.TermOption.Term / opt.TermOption.PeriodGap
		installment_rounded, _ := opt.GetInstallment()

		eff_rate, _ := finance.Rate(numPeriods, installment_rounded, -pv, fv, opt.TermOption.PaymentType, 1)
		eff_rate = eff_rate * 12 / float64(opt.TermOption.PeriodGap)
		eff_rate = ToFixed(eff_rate, 12)
		return eff_rate, nil
	}
	return 0, errors.New("Get irr rate failed.")
}

func (opt *PaymentSchuduleOption) GetInstallment() (float64, error) {

	if opt.Rate.EffectiveRate > 0 { // effective rate

		rate := opt.Rate.EffectiveRate / 100 / (12 / float64(opt.TermOption.PeriodGap))
		pv, _ := opt.GetPV()
		fv, _ := opt.GetFV()

		installment, _ := finance.Payment(rate,
			opt.TermOption.Term/opt.TermOption.PeriodGap,
			-pv,
			fv,
			opt.TermOption.PaymentType)

		installment_rounded := finance.Ceiling(installment, float64(opt.LeaseTypeSetup.RoundPayment))

		return installment_rounded, nil
	} else if opt.Rate.FlatRate > 0 { // flat rate
		pv, _ := opt.GetPV()
		fv, _ := opt.GetFV()
		installment := ((pv - fv) * float64(opt.TermOption.PeriodGap) / float64(opt.TermOption.Term)) + (float64(opt.Rate.FlatRate)/100*pv*float64(opt.TermOption.Term)/12)*float64(opt.TermOption.PeriodGap)/float64(opt.TermOption.Term)

		installment = ToFixed(installment, 2)

		installment_rounded := finance.Ceiling(installment, float64(opt.LeaseTypeSetup.RoundPayment))
		return installment_rounded, nil
	}

	return 0, errors.New("Get installment failed.")
}

func (opt *PaymentSchuduleOption) GetPaymentSchedule() ([]PaymentSchedule, error) {
	paymentSchedules := make([]PaymentSchedule, 0)
	pv, _ := opt.GetPV()
	fv, _ := opt.GetFV()

	irr_value, _ := opt.GetIrrRate()
	lineType := "Installment"
	outstanding := pv
	installment_rounded, _ := opt.GetInstallment()
	tax_amount := installment_rounded * opt.TaxOption.InstallmentTax / 100
	adjusted_net_payment_amount := installment_rounded + tax_amount

	if opt.TermOption.PaymentType == PaymentType.Ending {
		for i := 0; i <= opt.TermOption.Term; i++ {
			if i == 0 {
				ar_amount := adjusted_net_payment_amount*(float64(opt.TermOption.Term)/float64(opt.TermOption.PeriodGap)-0) + fv*(1+opt.TaxOption.InstallmentTax/100)
				paymentSchedules = append(paymentSchedules, PaymentSchedule{
					Period:               i,
					LineType:             lineType,
					OutstandingPrincipal: ToFixed(outstanding, 2),
					ARAmount:             ToFixed(finance.Round(ar_amount, 2), 2),
				})
			} else if i == opt.TermOption.Term { // last row

				payment := 0.0
				if opt.LeaseTypeSetup.CalSheetTax == false {
					payment = installment_rounded + opt.PaymentOption.BalloonAmount
				} else {
					payment = installment_rounded + opt.PaymentOption.BalloonAmount/(1+opt.TaxOption.InstallmentTax/100)
				}

				interest := installment_rounded + fv - outstanding
				pricipal := payment - interest
				outstanding = outstanding - pricipal
				paymentSchedules = append(paymentSchedules, PaymentSchedule{
					Period:                 i,
					LineType:               lineType,
					Payment:                ToFixed(payment, 2),
					Interest:               ToFixed(interest, 2),
					Principal:              ToFixed(pricipal, 2),
					OutstandingPrincipal:   ToFixed(outstanding, 2),
					ARAmount:               0,
					TaxAmount:              0,
					PaymentIncludeSalesTax: 0,
					WHTAmount:              0,
				})
			} else if i%opt.TermOption.PeriodGap == 0 { // not 1 and last

				interest := outstanding * irr_value / (12.0 / float64(opt.TermOption.PeriodGap))
				pricipal := installment_rounded - interest
				outstanding = outstanding - pricipal
				paymentSchedules = append(paymentSchedules, PaymentSchedule{
					Period:                 i,
					LineType:               lineType,
					Payment:                ToFixed(installment_rounded, 2),
					Interest:               ToFixed(interest, 2),
					Principal:              ToFixed(pricipal, 2),
					OutstandingPrincipal:   ToFixed(outstanding, 2),
					ARAmount:               0,
					TaxAmount:              0,
					PaymentIncludeSalesTax: 0,
					WHTAmount:              0,
				})
			} else {
				paymentSchedules = append(paymentSchedules, PaymentSchedule{
					Period:                 i,
					LineType:               lineType,
					Payment:                0,
					Interest:               0,
					Principal:              0,
					OutstandingPrincipal:   ToFixed(outstanding, 2),
					ARAmount:               0,
					TaxAmount:              0,
					PaymentIncludeSalesTax: 0,
					WHTAmount:              0,
				})
			}

		}
		return paymentSchedules, nil
	} else {
		for i := 1; i <= opt.TermOption.Term; i++ {
			if i == 1 {
				ar_amount := adjusted_net_payment_amount*(float64(opt.TermOption.Term)/float64(opt.TermOption.PeriodGap)-0) + fv*(1+opt.TaxOption.InstallmentTax/100)
				payment := 0.0
				interest := 0.0
				if opt.LeaseTypeSetup.CalSheetTax == false {
					payment = installment_rounded + opt.PaymentOption.BalloonAmount
				} else {
					payment = installment_rounded + opt.PaymentOption.BalloonAmount/(1+opt.TaxOption.InstallmentTax/100)
				}
				pricipal := payment - interest
				outstanding = outstanding - pricipal
				paymentSchedules = append(paymentSchedules, PaymentSchedule{
					Period:               i,
					LineType:             lineType,
					Payment:              payment,
					Interest:             0,
					Principal:            pricipal,
					OutstandingPrincipal: ToFixed(outstanding, 2),
					ARAmount:             ToFixed(finance.Round(ar_amount, 2), 2),
				})
			} else if i%opt.TermOption.PeriodGap == 1 || opt.TermOption.PeriodGap == 1 {

				interest := outstanding * irr_value / (12.0 / float64(opt.TermOption.PeriodGap))
				pricipal := installment_rounded - interest
				outstanding = outstanding - pricipal
				paymentSchedules = append(paymentSchedules, PaymentSchedule{
					Period:                 i,
					LineType:               lineType,
					Payment:                ToFixed(installment_rounded, 2),
					Interest:               ToFixed(interest, 2),
					Principal:              ToFixed(pricipal, 2),
					OutstandingPrincipal:   ToFixed(outstanding, 2),
					ARAmount:               0,
					TaxAmount:              0,
					PaymentIncludeSalesTax: 0,
					WHTAmount:              0,
				})
			} else {
				paymentSchedules = append(paymentSchedules, PaymentSchedule{
					Period:                 i,
					LineType:               lineType,
					Payment:                0,
					Interest:               0,
					Principal:              0,
					OutstandingPrincipal:   ToFixed(outstanding, 2),
					ARAmount:               0,
					TaxAmount:              0,
					PaymentIncludeSalesTax: 0,
					WHTAmount:              0,
				})
			}

		}
		return paymentSchedules, nil
	}

}

func getRound(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func ToFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(getRound(num*output)) / output
}
