package model

type PaymentSchuduleOption struct {
	LeaseTypeSetup  LeaseTypeSetup
	PaymentOption   PaymentOption
	AdditionalsCost AdditionalsCost
	TaxOption       TaxOption
	TermOption      TermOption
	Rate            Rate
}

type Rate struct {
	FlatRate      float64
	EffectiveRate float64
}

type LeaseTypeSetup struct {
	CalSheetTax     bool
	CalSheetAddCost bool
	FvFormular      string
	PvFormular      string
	CalSheetRate    string
	RoundMethod     string
	RoundType       string
	RoundPayment    int
}

type PaymentOption struct {
	DownPercent       float64
	DepositPercent    float64
	RVPercent         float64
	BalloonPercent    float64
	RepurchasePercent float64

	AdvancePayment   float64
	DownAmount       float64
	DepositAmount    float64
	RVAmount         float64
	BalloonAmount    float64
	RepurchaseAmount float64
}

type TaxOption struct {
	CostTax        float64
	InstallmentTax float64
	WHT            float64
	CostFundRate   float64
}

type TermOption struct {
	Term        int
	PaymentType int
	PeriodGap   int
}

type AdditionalsCost struct {
	VehicleTaxFee float64
	PropertyCost  float64
	Costs         []Cost
}

type Cost struct {
	CostType   string
	CostAmount float64
}

type PaymentSchedule struct {
	Period                 int
	LineType               string
	Payment                float64
	Interest               float64
	Principal              float64
	OutstandingPrincipal   float64
	ARAmount               float64
	TaxAmount              float64
	PaymentIncludeSalesTax float64
	WHTAmount              float64
}

// #region FvFormular
var FvFormular = newFVFormular()

func newFVFormular() *fvFormular {
	return &fvFormular{
		RV:         "RV",
		RVDeposite: "RVDeposite",
		Repurchase: "Repurchase",
		Balloon:    "Balloon",
	}
}

type fvFormular struct {
	RV         string
	RVDeposite string
	Repurchase string
	Balloon    string
}

// #endregion

// #region PvFormular
var PvFormular = newPvFormular()

func newPvFormular() *pvFormular {
	return &pvFormular{
		Cost:         "Cost",
		CostDeposite: "CostDeposite",
	}
}

type pvFormular struct {
	Cost         string
	CostDeposite string
}

// #endregion

// #region CalSheetRate
var CalSheetRate = newCalSheetRate()

func newCalSheetRate() *calSheetRate {
	return &calSheetRate{
		EffectiveRate: "EffectiveRate",
		FlatRate:      "FlatRate",
	}
}

type calSheetRate struct {
	EffectiveRate string
	FlatRate      string
}

// #endregion

// #region Round
var Round = newRound()

func newRound() *round {
	return &round{
		None:      "None",
		RoundUp:   "RoundUp",
		RoundDown: "RoundDown",
	}
}

type round struct {
	None      string
	RoundUp   string
	RoundDown string
}

// #endregion

// #region PaymentType
var PaymentType = newPaymentType()

func newPaymentType() *paymenttype {
	return &paymenttype{
		Ending:    0,
		Beginning: 1,
	}
}

type paymenttype struct {
	Ending    int
	Beginning int
}

// #endregion

// #region PaymentGap
var PeriodGap = newPeriodGap()

func newPeriodGap() *periodgap {
	return &periodgap{
		Monthly:      1,
		Quarterly:    3,
		SemiAnnually: 6,
		Annually:     12,
	}
}

type periodgap struct {
	Monthly      int
	Quarterly    int
	SemiAnnually int
	Annually     int
}

// #endregion
